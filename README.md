## Project Setup

cd into /api then

``` npm install ```

and then 

``` npm run dev ```

to start the api server.

cd into /client then

``` npm install ```

and then

``` npm run serve ```

to start the client server.