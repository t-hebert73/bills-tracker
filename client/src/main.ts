import Vue from 'vue';
import Buefy from 'buefy';
import VueSweetalert2 from 'vue-sweetalert2';
import dayjs from 'dayjs';
import App from './App.vue';
import router from './router';
import store from './store';
import './assets/bulma.scss';
import Bills from './api/Bills';

import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSweetalert2);

Vue.use(Buefy);
Vue.config.productionTip = false;

const bills: Bills = new Bills();
Vue.prototype.$api = {
  bills,
};

Vue.prototype.dayjs = dayjs;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
