import Vue from 'vue';
import Bills from '@/api/Bills';

declare module 'vue/types/vue' {
  interface Vue {
    $api: {
      bills: Bills;
    };
  }
}
