import Axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';

class BaseApi {
  public axios: AxiosInstance

  public axiosConfig: AxiosRequestConfig = {
    baseURL: '/api/',
  }

  constructor() {
    this.axios = Axios.create(this.axiosConfig);
  }

  /**
   * @param resource - the resource url
   * @param queryParams - the query params
   */
  public async get(resource: string, queryParams?: object): Promise<AxiosResponse> {
    return this.axios({
      method: 'get',
      url: resource,
      params: queryParams,
    }).then((res) => res);
  }

  /**
   * @param resource - the resource url
   * @param postData - the post data
   */
  public async post(resource: string, postData: object) {
    return this.axios({
      method: 'post',
      url: resource,
      data: postData,
    }).then((res) => res.data);
  }

  /**
   * @param resource - the resource url
   * @param patchData - the patch data
   */
  public async patch(resource: string, patchData: object) {
    return this.axios({
      method: 'patch',
      url: resource,
      data: patchData,
    }).then((res) => res.data);
  }

  /**
   * @param resource - the resource url
   * @param queryParams - the query params
   */
  public async delete(resource: string, queryParams?: object): Promise<AxiosResponse> {
    return this.axios({
      method: 'delete',
      url: resource,
      params: queryParams,
    }).then((res) => res.data);
  }
}

export default BaseApi;
