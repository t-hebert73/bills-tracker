import BaseApi from './BaseApi';

class Bills extends BaseApi {
  /**
   * @param queryParams -
   */
  public async all(queryParams?: object) {
    return this.get('bills', queryParams).then((res) => res.data);
  }

  /**
   * @param postData -
   */
  public async create(postData: object) {
    return this.post('bills', postData).then((data) => data);
  }

  /**
   * @param id -
   * @param queryParams -
   */
  public async update(id: number, updateData: object) {
    return this.patch(`bills/${id}`, updateData).then((data) => data);
  }

  /**
   * @param id -
   * @param queryParams -
   */
  public async destroy(id: number, queryParams?: object) {
    return this.delete(`bills/${id}`, queryParams).then((res) => res.data);
  }

  /**
   * @param id -
   * @param queryParams -
   */
  public async find(id: number, queryParams?: object) {
    return this.get(`bills/${id}`, queryParams).then((res) => res.data);
  }
}

export default Bills;
