import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/bills/add',
    name: 'CreateBill',
    // route level code-splitting
    // this generates a separate chunk (create-bill.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "create-bill" */ '../views/CreateBill.vue'),
  },
  {
    path: '/bills/:id',
    name: 'ViewBill',
    // route level code-splitting
    // this generates a separate chunk (view-bill.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "view-bill" */ '../views/ViewBill.vue'),
  },
  {
    path: '/bills/:id/edit',
    name: 'EditBill',
    // route level code-splitting
    // this generates a separate chunk (edit-bill.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "edit-bill" */ '../views/EditBill.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
