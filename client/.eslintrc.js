module.exports = {
  root: true,
  env: {
    node: true,
  },
  plugins: [
    'eslint-plugin-tsdoc'
  ],
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
    '@vue/typescript/recommended'
  ],
  parserOptions: {
    ecmaVersion: 2020,
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'tsdoc/syntax': 'warn',
    'max-len': [2, 150]
    
  },
};
