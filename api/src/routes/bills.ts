import Router from '@koa/router';
import { getConnection } from 'typeorm';
import Bill from '../entities/Bill';

const router = new Router();

router.get('/api/bills', async (ctx: any) => {
  const connection = getConnection();
  const billRepository = connection.getRepository(Bill);
  const allBills = await billRepository.find();
  // console.log("All bills from the db: ", allBills);

  ctx.body = allBills;
});

router.post('/api/bills', async (ctx: any) => {
  const connection = getConnection();
  const bill: Bill = new Bill();

  bill.name = ctx.request.body.name;
  bill.provider = ctx.request.body.provider;
  bill.category = ctx.request.body.category;
  bill.frequency = ctx.request.body.frequency;
  bill.automaticPayment = ctx.request.body.automaticPayment;
  bill.requiresRenewal = ctx.request.body.requiresRenewal;

  await connection.manager.save(bill);

  ctx.body = bill;
});

router.get('/api/bills/:id', async (ctx: any) => {
  const connection = getConnection();
  const billRepository = connection.getRepository(Bill);
  const bill: Bill = await billRepository.findOne({
    where: { id: ctx.params.id },
    relations: ['payments'],
  });

  ctx.body = bill;
});

router.patch('/api/bills/:id', async (ctx: any) => {
  const connection = getConnection();
  const billRepository = connection.getRepository(Bill);
  const bill: Bill = await billRepository.findOne(ctx.params.id);

  bill.name = ctx.request.body.name;
  bill.provider = ctx.request.body.provider;
  bill.category = ctx.request.body.category;
  bill.frequency = ctx.request.body.frequency;
  bill.automaticPayment = ctx.request.body.automaticPayment;
  bill.requiresRenewal = ctx.request.body.requiresRenewal;

  await connection.manager.save(bill);

  ctx.body = bill;
});

router.delete('/api/bills/:id', async (ctx: any) => {
  const connection = getConnection();
  const billRepository = connection.getRepository(Bill);
  const bill: Bill = await billRepository.findOne(ctx.params.id);

  await connection.manager.remove(bill);

  ctx.body = 'success';
});

export default router;
