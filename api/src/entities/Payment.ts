import {
  Entity, Column, PrimaryGeneratedColumn, ManyToOne,
} from 'typeorm';
import Bill from './Bill';

@Entity({
  name: 'payments',
})
export default class Payment {
  @PrimaryGeneratedColumn({ unsigned: true })
  id: number;

  @ManyToOne(() => Bill, (bill) => bill.paymentHistory)
  bill: Bill;

  @Column()
  amount: number;

  @Column({
    nullable: true,
  })
  date: Date;

  @Column({
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  created: Date;
}
