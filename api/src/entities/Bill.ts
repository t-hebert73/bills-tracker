import {
  AfterLoad, Entity, Column, PrimaryGeneratedColumn, OneToMany,
} from 'typeorm';
import Payment from './Payment';

@Entity({
  name: 'bills',
})
export default class Bill {
  @PrimaryGeneratedColumn({ unsigned: true })
  id: number;

  @OneToMany(() => Payment, (payment) => payment.bill)
  payments: Payment[];

  @Column({
    length: 100,
  })
  name: string;

  @Column({
    length: 100,
  })
  category: string;

  @Column({
    length: 100,
  })
  frequency: string;

  @Column({
    default: false,
  })
  automaticPayment: boolean;

  @Column({
    default: false,
  })
  requiresRenewal: boolean;

  @Column({
    length: 100,
  })
  provider: string;

  @Column({
    nullable: true,
  })
  lastPaid: Date;

  @Column({
    nullable: false,
    default: () => 'CURRENT_TIMESTAMP',
  })
  created: Date;

  isPaidThisMonth: boolean = false;

  @AfterLoad()
  updateIfIsPaidThisMonth() {
    if (this.lastPaid) {
      const today = new Date();

      if (
        this.lastPaid.getFullYear() === today.getFullYear()
        && this.lastPaid.getMonth() === today.getMonth()) {
        this.isPaidThisMonth = true;
      }
    }
  }
}
