import Koa from 'koa';
import KoaRouter from '@koa/router';
// const cors = require('@koa/cors')

import 'reflect-metadata';
import { createConnection } from 'typeorm';
import Bill from './entities/Bill';
import Payment from './entities/Payment';

import billsRouter from './routes/bills';

const serve = require('koa-static');
const path = require('path');
const bodyParser = require('koa-bodyparser');

const main = async () => {
  const connection = await createConnection({
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    password: 'admin',
    database: 'bills_tracker',
    entities: [
      Bill, Payment,
    ],
    synchronize: true,
    logging: true,
  });

  const app = new Koa();
  const router = new KoaRouter();

  app.use(bodyParser());

  // logger

  app.use(async (ctx: any, next: any) => {
    await next();
    const rt = ctx.response.get('X-Response-Time');
    console.log(`${ctx.method} ${ctx.url} - ${rt}`);
  });

  // x-response-time

  app.use(async (ctx: any, next: any) => {
    const start = Date.now();
    await next();
    const ms = Date.now() - start;
    ctx.set('X-Response-Time', `${ms}ms`);
  });

  // this allows / to load in index.html.. but any other route doesnt work
  app.use(serve(path.join(__dirname, '../../client/dist')));

  app.use(billsRouter.routes());
  app.use(billsRouter.allowedMethods());

  app.use(router.routes()).use(router.allowedMethods());

  app.listen(3000);
};

main();
